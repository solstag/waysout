"""
ME: Modalities of Existence
DS: Domains
"""

import pandas as pd


COLUMNS_ME = ["What", "Why", "How"]


def parse_col(text):
    return set(item for item in text.split("\n") if item)


def parse_table(data_text, columns_text=None):
    return pd.DataFrame(
        data=(line.split("\t") for line in data_text.split("\n")),
        columns=(columns_text.split("\t") if columns_text else None),
    )


def get_text_me():
    """
    Because <https://debbugs.gnu.org/cgi/bugreport.cgi?bug=59421>:
    Copy table from spreadsheet, then eval (C-x C-e):
    (progn
      (switch-to-buffer-other-window "*Python*")
      (set-window-point nil (+ (buffer-size) 1))
      (yank (list 4))
      (while (search-forward "\t" nil t)
        (replace-match "\\t" nil t))
      (set-window-point nil (+ (buffer-size) 1))
    )
    """
    text_me = []
    line = input("Paste table here! (Stops at blank line)\n")
    while line != "":
        text_me.append(line.replace("\\t", "\t"))
        line = input()
    return "\n".join(text_me)


def get_markdown_ds2me(ds2me, kind):
    ds2me_kind = ds2me.loc[ds2me.index.get_level_values(0).map(len) == kind]
    pretty_index = ds2me_kind.index.get_level_values(0).map(lambda y: " & ".join(y))
    ds2me_kind_pretty = ds2me_kind.set_index(pretty_index)
    return ds2me_kind_pretty.to_markdown()


def get_markdown_table(ds2me, kind):
    return (
        f'<div class="table-font-{kind}0">\n\n'
        + get_markdown_ds2me(ds2me, kind)
        + "\n\n</div>"
    )


def print_slides(ds2me):
    for kind in (1, 2):
        print("\n\n----\n\n")
        print('## Modalities of existence: "Three farmers" constellation kernel\n')
        print(get_markdown_table(ds2me, kind))


def get_me(text_me, columns_me=COLUMNS_ME, how_before_comma=False):
    text_columns = ["cell"] + columns_me
    me = parse_table(text_me, "\t".join(text_columns))
    me = me.loc[~me[columns_me].eq(len(columns_me) * ["?"]).all(axis=1)]
    me["L1"] = me["cell"].map(lambda x: {"2": "D78", "3": "D66", "4": "D89"}[x[1]])
    if how_before_comma:
        me["How"] = me.How.str.split(",").map(lambda x: x[0])
    return me


def get_ds2me(me, columns_me=COLUMNS_ME):
    ds2me = (
        me.groupby(columns_me)
        .agg(lambda x: tuple(sorted({*x})))
        .sort_values("L1", kind="stable")
    )
    ds2me = ds2me.index.to_frame().set_index(
        [ds2me[col] for col in reversed(ds2me.columns)]
    )
    return ds2me


try:
    text_me = text_me
except NameError:
    text_me = get_text_me()
me = get_me(text_me)
ds2me = get_ds2me(me)
print_slides(ds2me)
