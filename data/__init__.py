import json
from pathlib import Path

from bibliodbs.query_downloader import (
    download_queries,
    filter_jsonl_dataset,
    merge_jsonl_datasets,
)
from bibliodbs.biblio_wos import extract_record

from . import queries


DATA_DIR = Path("data")


#####################################
# Data downloading and delimitation #
#####################################


def download_waysout_data(api_key):
    waysout_queries = {name: getattr(queries, name) for name in waysout_query_names()}
    download_queries(
        waysout_queries,
        DATA_DIR,
        api_key,
        wosquerydownloader_args={"edition": "WOS SSCI"},
    )
    filter_jsonl_dataset(
        make_category_filter(waysout_destabilization_deny_cats()),
        DATA_DIR / "monogrammes_dest.jsonl",
        DATA_DIR / "monogrammes_dest-fixdest.jsonl",
    )
    for name in ("monogrammes_phas", "monogrammes_dest-fixdest"):
        filter_jsonl_dataset(
            make_category_filter(waysout_monogrammes_deny_cats()),
            DATA_DIR / f"{name}.jsonl",
            DATA_DIR / f"{name}-fixmono.jsonl",
        )
    merge_jsonl_datasets(
        [DATA_DIR / f"{name}.jsonl" for name in waysout_query_names(filtered=True)],
        DATA_DIR / "waysout_filtered_and_merged.jsonl",
        ["UID"],
    )


def waysout_query_names(filtered=False):
    return (
        [
            "core",
            "extended_broken_00",
            "extended_broken_01",
            "extended_broken_10",
            "extended_broken_11",
            "monogrammes_phas",
            "monogrammes_dest",
        ]
        if not filtered
        else [
            "core",
            "extended_broken_00",
            "extended_broken_01",
            "extended_broken_10",
            "extended_broken_11",
            "monogrammes_phas-fixmono",
            "monogrammes_dest-fixdest-fixmono",
        ]
    )


def waysout_destabilization_deny_cats():
    return ["Rehabilitation"]


def waysout_monogrammes_deny_cats():
    return [
        "Psychology, Clinical",
        "Psychology, Experimental",
        "Psychiatry",
        "Psychoanalysis",
        "Neurosciences",
        "Clinical Neurology",
        "Geriatrics & Gerontology",
        "Cell Biology",
        "Biochemistry & Molecular Biology",
    ]


def make_category_filter(deny_cats):
    deny_cats = set(deny_cats)

    def category_filter(obj):
        record_meta = obj["static_data"]["fullrecord_metadata"]
        record_cats = (
            record_meta["category_info"]["subjects"]["subject"]
            if "category_info" in record_meta
            else []
        )
        for cat_data in record_cats:
            if (
                cat_data["ascatype"] == "traditional"
                and cat_data["content"] in deny_cats
            ):
                return False
        return True

    return category_filter


################
# Data loading #
################


def load_waysout_data(extract=True):
    data_path = DATA_DIR / "waysout_filtered_and_merged.jsonl"
    with data_path.open() as f:
        for line in f:
            obj = json.loads(line)
            yield obj if not extract else extract_record(obj)


def load_waysout_data_indexed(extract=True):
    uid_key = "uid" if extract else "UID"
    return {record.pop(uid_key): record for record in load_waysout_data(extract)}


###################
# Data processing #
###################


def clean_data(df, col):
    bad_uids = set(["WOS:000415022000001"])
    df = df[~df["uid"].isin(bad_uids)]
    df = df[df[col].astype(bool)]
    return df


def filter_bad_terms(corpus):
    bad_terms = {
        "nt",  # polyssemic acronym
        "all_rights_reserved",  # elsevier has no scruples
        "sic_sic_sic_sic",  # chinese in doi:10.1080/08111146.2020.1730786
        "this_article",  # style feature hiding 'institutions'
    }
    return corpus.filter_terms(lambda x: x not in bad_terms)


def add_extended_data(corpus, prop, name):
    data = load_waysout_data_indexed()
    corpus.odata[name] = corpus.odata["uid"].map(lambda uid: data[uid][prop])
    corpus.data[name] = corpus.data["uid"].map(lambda uid: data[uid][prop])
