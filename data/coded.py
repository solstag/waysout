#!/usr/bin/env python

import pandas as pd

from sashimi.blocks import annotations

CODING_PATH = "./codage/Codage SASHIMI destab_ext2 L1D - Consolidated.csv"

# Keep these two in sync
PERTINENCES_NUM = {"Yes": 1, "No": 0, "Context": 2, "Total": 1, "Maybe": 3}
NUMS_PERTINENCE = {0: "No", 1: "Yes|Total", 2: "Context", 3: "Maybe"}


def get_coding(coding_path=CODING_PATH):
    coding = pd.read_csv(coding_path, index_col="L1D")
    return coding


def get_docs_pertinence(
    corpus,
    coding=None,
    pertinences_num=PERTINENCES_NUM,
):
    if coding is None:
        coding = get_coding()
    coding = coding.copy()
    coding.index = coding.index.map(lambda x: corpus.label_to_tlblock[f"L1D{x}"][-1])
    blocks_pertinence = coding["Pertinence"].map(pertinences_num.get)
    docs_pertinence = corpus.dblocks[1].map(blocks_pertinence.get)
    return docs_pertinence


def get_pertinence_selectors(corpus, coding=None):
    if coding is None:
        coding = get_coding()
    pertinences_label = {}
    for key in ("Yes", "No"):
        pertinences_label[key] = set(coding.index[coding["Pertinence"].eq(key)])
    pertinences_label["Context"] = set(
        coding.index[coding["Other"].str.contains(r"\b#context\b")]
    )
    pertinences__b = {}
    for key, labels in pertinences_label.items():
        pertinences__b[key] = corpus.dblocks[1].isin(
            set(corpus.label_to_hblock[f"L1D{x}"][-1] for x in labels)
        )
    return pertinences__b


########################
# Visualize pertinence #
########################

def get_pertinence_yblocks_elements(
    corpus,
    pertinence_blocks,
    ybtype,
    ylevel,
    num=10,
    ent_frac=1 / 2,
):
    orig_dblocks, orig_dblocks_levels = (
        corpus.dblocks.copy(),
        corpus.dblocks_levels.copy(),
    )
    try:
        corpus.dblocks.drop(columns=corpus.dblocks_levels)
        corpus.dblocks_levels = [1, 2]
        corpus.dblocks[2] = 0
        corpus.dblocks[1] = pertinence_blocks
        return {
            xb: annotations.get_xblock_yblocks_elements(
                corpus, "doc", 1, xb, ybtype, ylevel, num, ent_frac
            )
            for xb in corpus.dblocks[1].unique()
        }
    finally:
        corpus.dblocks, corpus.dblocks_levels = orig_dblocks, orig_dblocks_levels


def format_pertinence_yblock_elements(corpus, pertinence_yblocks_elements):
    def get_yblock_label(ybtype, ylevel, yblock):
        return corpus.hblock_to_label[
            corpus.level_block_to_hblock(ylevel, yblock, ybtype)
        ]

    lines = []
    for pertinence, pertinence_value in pertinence_yblocks_elements.items():
        lines.append(f"{NUMS_PERTINENCE[pertinence]}:")
        ybtype, ylevel = pertinence_value["btypes"][1], pertinence_value["levels"][1]
        for topic_index, topic_value in pertinence_value["blocks"].items():
            yblock_label = get_yblock_label(ybtype, ylevel, topic_index)
            lines.append(f"\t{yblock_label} ({topic_value['ent']}):")
            for term, entropy in topic_value["elements"]:
                lines.append(f"\t\t{term} ({entropy})")
    return lines


def print_pertinence_topics_terms(corpus):
    coding = get_coding()
    pertinence_blocks = get_docs_pertinence(corpus, coding)
    pertinence_topics_terms = get_pertinence_yblocks_elements(
        corpus, pertinence_blocks, "ter", 1
    )
    for line in format_pertinence_yblock_elements(corpus, pertinence_topics_terms):
        print(line)
