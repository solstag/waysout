constellations = dict(
    constellation_farmers=[
        "L1D66",
        "L1D78",
        "L1D89",
        "L1D47",
        "L1D48",
        "L1D85",
        "L1D67",
        "L1D69",
    ],
    constellation_regimes=[
        "L1D25",
        "L1D28",
        "L1D29",
        "L1D30",
        "L1D32",
        "L1D33",
        "L1D34",
        "L1D36",
        "L1D5",
        "L1D64",
        "L1D69",
    ],
)

domains_titles = {}

# Farmers

domains_titles["L1D66"] = [
    "Food crises, food regimes and food movements: rumblings of reform or tides of transformation?",
    "TOWARDS A REGIME CHANGE IN THE ORGANIZATION OF THE SEED SUPPLY SYSTEM IN CHINA",
    "The need for policy to address the food system lock-in: A case study of the Finnish context",
    "Disciplining the State: The role of alliances in contesting multi-level agri-environmental governance",
    "The Evolution of Problems Underlying the EU Agricultural Policy Regime",
    'The Role of Law in Transformative Environmental PoliciesA Case Study of "Timber in Buildings Construction in Germany"',
    "Endogenous regime change: Lessons from transition pathways in Dutch dairy farming",
    "Politicizing African urban food systems: The contradiction of food governance in Rabat and Casablanca, Morocco",
    "Going beyond definitions to understand tensions within the bioeconomy: The contribution of sociotechnical regimes to contested fields",
    "Change as a permanent condition: A history of transition processes in Dutch North Sea fisheries",
    "How food systems change (or not): governance implications for system transformation processes",
    "Just wheat transitions?: working toward constructive structural changes in wheat production",
] + [
    "Transition challenges in consumer acculturation: Role destabilization and changes in symbolic consumption",
]

domains_titles["L1D78"] = [
    "THE ECONOMICS OF PESTICIDE USE AND REGULATION",
    "DYNAMICS OF STRUCTURAL-CHANGE IN THE ONTARIO HOG INDUSTRY",
    "THE ECONOMIC CONSEQUENCES OF REDUCED FERTILIZER USE - A VIRTUAL PRICING APPROACH",
    "European environmental regulations to reduce water pollution: An analysis of their impact on UK dairy farms",
    "Restricting intensive livestock production: Economic effects of mineral policy in the Netherlands",
    "The methyl bromide ban: Economic impacts on the California strawberry industry",
    "No-till technology: benefits to farmers and the environment? Theoretical analysis and application to Finnish agriculture",
    "Phasing out of environmentally harmful subsidies: Consequences of the 2003 CAP reform",
    "Would banning atrazine benefit farmers?",
] + []

domains_titles["L1D89"] = [
    "Why are ecological, low-input, multi-resistant wheat cultivars slow to develop commercially? A Belgian agricultural 'lock-in' case study",
    "Scaling down the European model of agriculture: the case of the Rural Environmental Protection Scheme in Ireland",
    "Runoff and soil erosion in arable Britain: changes in perception and policy since 1945",
    "Special Interests, Regulatory Quality, and the Pesticides Overload",
    "The Effect of Mandatory Agro-Environmental Policy on Farm Fertiliser and Pesticide Expenditure",
    "Responding to environmental regulations through collaborative arrangements: Social aspects of manure partnerships in Denmark",
    "A Review of the Global Pesticide Legislation and the Scale of Challenge in Reaching the Global Harmonization of Food Safety Standards",
    "Reducing deforestation and enhancing sustainability in commodity supply chains: interactions between governance interventions and cattle certification in Brazil",
    "Transitional pathways towards input reduction on French field crop farms",
    "Why are grain-legumes rarely present in cropping systems despite their environmental and nutritional benefits? Analyzing lock-in in the French agrifood system",
    "Pesticide lock-in in small scale Peruvian agriculture",
    "Ecophyto, the French action plan to reduce pesticide use: a failure analyses and reasons for hoping",
    "Agriculture after Brexit",
    "Socio-technical lock-in hinders crop diversification in France",
    "Lock-ins and Agency: Towards an Embedded Approach of Individual Pathways in the Walloon Dairy Sector",
    "The abandonment of maize landraces over the last 50 years in Morelos, Mexico: a tracing study using a multi-level perspective",
    "Cultural Lock-in and Mitigating Greenhouse Gas Emissions: The Case of Dairy/Beef Farmers in Norway",
] + [
    "Impacts of human behaviour in agri-environmental policies: How adequate is homo oeconomicus in the design of market-based conservation instruments?",
    "More than two decades of Agri-Environment schemes: Has the profile of participating farms changed?",
    "Factors underlying farmers' intentions to perform unsubsidised agri-environmental measures",
]

domains_titles["L1D47"] = [
    "Decarbonizing the boardroom? Aligning electric utility executive compensation with climate change incentives",
    "Strategic investment decisions under the nuclear power debate in Belgium",
    "Coal phase-outs and carbon prices: Interactions between EU emission trading and national carbon mitigation policies",
    "Exploring investment potential in a context of nuclear phase-out uncertainty: Perfect vs. imperfect electricity markets",
    "The future of coal and renewable power generation in Australia: A review of market trends",
    "Nuclear decommissioning after the German Nuclear Phase-Out an integrated view on new regulations and nuclear logistics",
    "How to deal with the risks of phasing out coal in Germany",
    "Incumbent's Bane or Gain? Renewable Support and Strategic Behavior in Electricity Markets",
] + []


domains_titles["L1D48"] = [
    "THE IMPACT OF OIL COMPANY INVESTMENT ON THE WORLD COAL INDUSTRY - OVERCAPACITY AND PRICE DESTABILIZATION 1973-92",
    "THE ECONOMIC-IMPACT OF SUBSIDY PHASE-OUT IN OIL EXPORTING DEVELOPING-COUNTRIES - A CASE-STUDY OF ALGERIA, IRAN AND NIGERIA",
    "Jobs versus the environment: An industry-level perspective",
    "Moving to greener pastures? Multinationals and the pollution haven hypothesis",
    "OPEC: Market failure or power failure?",
    "International coal trade and restrictions on coal consumption",
    "The game of trading jobs for emissions",
    "The perverse fossil fuel subsidies in China-The scale and effects",
    "The oil endgame: Strategies of oil exporters in a carbon-constrained world",
    "Politics and petroleum: Unintended implications of global oil demand reduction policies",
    "Nuclear Phase-out Under Stringent Climate Policies: A Dynamic Macroeconomic Analysis",
    "The impact of phasing out fossil fuel subsidies on the low-carbon transition",
    "How the removal of producer subsidies influences oil and gas extraction: A case study in the Gulf of Mexico",
    "The Impact of Energy De-Subsidization Policy in 2030: A Dynamic CGE Model in China",
    "Low-carbon transition in a coal-producing country: A labour market perspective",
] + []

domains_titles["L1D85"] = [
    "CHALLENGES TO PHASING OUT FOSSIL FUELS AS THE MAJOR SOURCE OF THE WORLD'S ENERGY",
    "The decline of sectorial components of the world's energy intensity",
    "The Low-Carbon Transition toward Sustainability of Regional Coal-Dominated Energy Consumption Structure: A Case of Hebei Province in China",
    "Decoupling, decomposition and forecasting analysis of China's fossil energy consumption from industrial output",
    "Is carbon emission decline caused by economic decline? Empirical evidence from Russia",
    "Path Analysis of Beijing's Dematerialization Development Based on System Dynamics",
    "The path to a 2025 nuclear-free Taiwan: An analysis of dynamic competition among emissions, energy, and economy",
    "The Impacts of Technology Shocks on Sustainable Development from the Perspective of Energy Structure-A DSGE Model Approach",
] + [
    "Measurement on carbon lock-in of China based on RAGA-PP model",
    "Structural path and decomposition analysis of aggregate embodied energy and emission intensities",
]

domains_titles["L1D67"] = [
    "Escaping carbon lock-in",
    "From sectoral systems of innovation to socio-technical systems - Insights about dynamics and change from sociology and institutional theory",
    "Typology of sociotechnical transition pathways",
    "Ideas, institutions, and interests: explaining policy divergence in fostering 'system innovations' towards sustainability",
    "A dynamic conceptualization of power for sustainability research",
    "The shadowy side of innovation: unmaking and sustainability",
    "Transition failure: Understanding continuity in the automotive industry",
    "Socio-technical regimes and sustainability transitions: Insights from political ecology",
    "Up, down, round and round: connecting regimes and practices in innovation for sustainability",
    "The Social Dynamics of Degrowth",
    "Putting the Power in 'Socio-Technical Regimes' - E-Mobility Transition in China as Political Process",
    "Civil society organizations and deliberative policy making: interpreting environmental controversies in the deliberative system",
    "Importance of Actors and Agency in Sustainability Transitions: A Systematic Exploration of the Literature",
    "Shifting Power Relations in Sustainability Transitions: A Multi-actor Perspective",
    "The economic crisis as a game changer? Exploring the role of social construction in sustainability transitions",
    "Transforming power/knowledge apparatuses: the smart grid in the German energy transition",
    "Historical institutionalism and the politics of sustainable energy transitions: A research agenda",
    "Moving beyond the heuristic of creative destruction: Targeting exnovation with policy mixes for energy transitions",
    "Discontinuation of the automobility regime? An integrated approach to multi-level governance",
    "Power in Sustainability Transitions: Analysing power and (dis)empowerment in transformative change towards sustainability",
    "Towards a conceptualization of power in energy transitions",
    "Bridging socio-technical and justice aspects of sustainable energy transitions",
    "Sustainability transitions and the state",
    "Beyond the disruption narrative: Varieties and ambiguities of energy system change",
    "Global socio-technical regimes",
    "Information stabilisation and destabilisation as potential usable concepts in practice theoretical approaches",
    "The politics of accelerating low-carbon transitions: Towards a new research agenda",
    "Reconfiguration, Contestation, and Decline: Conceptualizing Mature Large Technical Systems",
    "Linking socio-technical transition studies and organisational change management: Steps towards an integrative, multi-scale heuristic",
    "What enables just sustainability transitions in agrifood systems? An exploration of conceptual approaches using international comparative case studies",
    "Redefining power relations in agrifood systems",
    "Roots, Riots, and Radical ChangeA Road Less Travelled for Ecological Economics",
    "Justice in energy transitions",
    "Sustainability through institutional failure and decline? Archetypes of productive pathways",
    "Radical change and deep transitions: Lessons from Europe's infrastructure transition 1815-2015",
    "Exploring the role of failure in socio-technical transitions research",
    "Leviathan Awakens: Gas Finds, Energy Governance, and the Emergence of the Eastern Mediterranean as a Geopolitical Region",
    "Transition topology: Capturing institutional dynamics in regional development paths to sustainability",
    "Towards a theory of just transition: A neo-Gramscian understanding of how to shift development pathways to zero poverty and zero carbon",
    "Studying Industrial Decarbonisation: Developing an Interdisciplinary Understanding of the Conditions for Transformation in Energy-Intensive Natural Resource-Based Industry",
    "Transition tensions: mapping conflicts in movements for a just and sustainable transition",
    "How to change the sources of meaning of resistance identities in historically coal-reliant mining communities",
    "Revisiting carbon lock-in in energy systems: Explaining the perpetuation of coal power in Japan",
    "Territorial and institutional obduracy in regional transition: politicising the case of Flanders' energy distribution system",
    "The politics of deliberate destabilisation for sustainability transitions",
    "Bringing geopolitics to energy transition research",
    "Regime resistance and accommodation: Toward a neo-Gramscian perspective on energy transitions",
    "The energy-extractives nexus and the just transition",
    "(Un)making in sustainability transformation beyond capitalism",
    "Rethinking the Multi-level Perspective for energy transitions: From regime life-cycle to explanatory typology of transition pathways",
    "Just transition: A conceptual review",
] + [
    "The politics of deliberate destabilisation for sustainability transitions",
    "User innovation, niche construction and regime destabilization in heat pump transitions",
]

domains_titles["L1D69"] = [
    "REGRESSION AS A CARDINAL FACTOR IN ORGANIZATIONAL DESTABILIZATION",
    "The dynamic implications of increasing returns: Technological change and path dependent inefficiency",
    "Divergence, sensitivity, and nonequilibrium in ecosystems",
    "Review essay - Family firms amidst the creative destruction of capitalism",
    "Never Waste a Good Crisis: An Historical Perspective on Comparative Corporate Governance",
    "Taming Prometheus: Talk About Safety and Culture",
    "On drifting rules and standards",
    "Why are we growth-addicted? The hard way towards degrowth in the involutionary western development path",
    "Reversals of fortune: path dependency, problem solving, and temporal cases",
    "Path Dependence and QWERTY's Lock-In: Toward a Veblenian Interpretation",
    "Organizational Path Dependence: A Process View",
    "When Organization Studies Turns to Societal Problems: The Contribution of Marxist Grand Theory",
    "Stable and destabilised states of subjective well-being: dance and movement as catalysts of transition",
    "Things Fall Apart: The Dynamics of Brand Audience Dissipation",
    "The evolution of the law of indirect expropriation and its application to oil and gas investments",
    "How deep is incumbency? A 'configuring fields' approach to redistributing and reorienting power in socio-material change",
    "The ontological politics of (in-)equality: a new research approach for post-development",
    "Ordoliberalism and Neoliberalization: Governing through Order or Disorder",
    "Defrosting concepts, destabilizing doxa: Critical phenomenology and the perplexing particular",
    'Rethinking the meaning of "landscape shocks" in energy transitions: German social representations of the Fukushima nuclear accident',
    "An exploration of earth system vulnerability in the context of landfills in the Anthropocene",
    "Carceral lock-in: How organizational conditions stymie the development of justice alternatives in a rape crisis center",
    '''Thinking Absence: A Discussion of "The Analyst's Necessary Nonsovereignty and the Generative Power of the Negative"''',
    "Sociology's encounter with the decolonial: The problematique of indigenous vs that of coloniality, extraversion and colonial modernity(1)",
    '"[This] system was not made for [you]:" A case for decolonial Scientia',
    "Ignorance as a productive response to epistemic perturbations",
    "The gathering anthropocene crisis",
    '"Coal [from Colombia] is our life". Bourdieu, the miners (after they are miners) and resistance in As Pontes',
] + [
    "REGRESSION AS A CARDINAL FACTOR IN ORGANIZATIONAL DESTABILIZATION",
    "ISSUE OF FAIRNESS IN STRATIFICATION STUDIES - DESTABILIZATION OF THE STATUS-QUO",
    "Stable and destabilised states of subjective well-being: dance and movement as catalysts of transition",
    "Defrosting concepts, destabilizing doxa: Critical phenomenology and the perplexing particular",
    "The Psychology of Leadership Destabilization: An Analysis of the 2016 US Presidential Debates",
]


# L2 Institutions/Politics/Regimes

domains_titles["L1D25"] = [
    "Institutional and policy responses to uncertainty in environmental policy: A comparison of Dutch and US styles",
    "Increasing returns, path dependence, and the study of politics",
    "Institutional stability and change: two sides of the same coin",
    "Breaking the path of institutional development? Alternatives to the new determinism",
    "Policy windows, policy change, and organizational learning: Watersheds in the evolution of watershed management",
    "Ideas, institutions, and policy change",
    "Paths of the Past or the Road Ahead? Path Dependency and Policy Change in Two Continental European Welfare States",
    "Understanding Policy Change as a Historical Problem",
    "How Do Fields Change? The Interrelations of Institutions, Networks, and Cognition in the Dynamics of Markets",
    "HOW DO CRISES LEAD TO CHANGE? Liberalizing Capital Controls in the Early Years of New Order Indonesia",
    "Banning the bulb: Institutional evolution and the phased ban of incandescent lighting in Germany",
    "ON MELTING SUMMITS: THE LIMITATIONS OF FIELD-CONFIGURING EVENTS AS CATALYSTS OF CHANGE IN TRANSNATIONAL CLIMATE POLICY",
    "A Conceptual Framework for the Comparative Analysis of Policy Change: Measurement, Explanation and Strategies of Policy Dismantling",
]

domains_titles["L1D26"] = [
    "Discretion and the erosion of community trust in planning: reflections on the post-political",
    "New horizons for old industrial areas: urban shrinkage and social capital in Blaenau Gwent, Wales",
    "Enabling environments for regime destabilization towards sustainable urban transitions in megacities: comparing Shanghai and Istanbul",
    "Contested energy futures, conflicted rewards? Examining low-carbon transition risks and governance dynamics in China's built environment",
    "Industrial destabilisation: The case of Rajajinagar, Bangalore",
    "Tensions in Urban Transitions. Conceptualizing Conflicts in Local Climate Policy Arrangements",
]

domains_titles["L1D28"] = [
    "Dis-embeddedness and de-classification: modernization politics and the Greek teacher unions in the 1990s",
    "Developing compliance and resistance: the state, transnational social movements and tribal peoples contesting India's Narmada project",
    "Fragile stability: State and society in democratic South Africa",
    "The politics of welfare state retrenchment: A literature review",
    "After Fidel: The US-Cuba System and the Key Mechanisms of Regime Change",
    "Transitions, Transformations and the Role of Elites",
    "A changing State or regime change ? On a few points of confusion in theory and sociology of the State.",
    'Three in One Unpacking the "Collapse" of the Soviet Union',
    "Social movements, protest movements and cross-ideological coalitions - the Arab uprisings re-appraised",
    "The dawn of the secular state? Heritage and identity in Swedish church and state debates 1920-1939",
    "Using Critical Junctures to Explain Continuity: The Case of State Milk in Neoliberal Chile",
    "Historical perspectives on democratic police reform: Institutional memory, narratives and ritual in the post-war Italian police, 1948-1963",
    "Revolution or Negotiated Regime Change? Structural Dynamics in the Process of Democratization. The Case of South Korea in the 1980s",
    "Brexit, existential anxiety and ontological (in)security",
    "How path-creating mechanisms and structural lock-ins make societies drift from democracy to authoritarianism",
    "Autocracy or Perestroika 2.0 Can Russia escape from its path dependence?",
    "COVID-19 and the Politics of Crisis",
    "Erosion or decay? Conceptualizing causes and mechanisms of democratic regression",
    "The Iraqi protest movement: social mobilization amidst violence and instability",
    "Unravelling democratic erosion: who drives the slow death of democracy, and how?",
    "The Cost of Regime Survival: Political Instability, Underdevelopment, and (Un)natural Disasters in Haiti Before the 2010 Earthquake",
]

domains_titles["L1D29"] = [
    "MOZAMBIQUE - DESTABILIZATION, STATE, SOCIETY AND SPACE",
    "SPACE TECHNOLOGY AS A FACTOR OF INTERNATIONAL STABILIZATION AND DESTABILIZATION",
    "The Afghanistan war and the breakdown of the Soviet Union",
    "The end of conscription in Europe?",
    "Iraqi Transitions: from regime change to state collapse",
    "How unstable? Volatility and the genuinely new parties in Eastern Europe",
    "Phased out - Far right parties in western Europe",
    "Neoliberalism as creative destruction",
    "Revolution, Reform, and Status Inheritance: Urban China, 1949-1996",
    "Measuring Revolution",
    "The Decline of a Dominant Party and the Destabilization of Electoral Authoritarianism?",
    "Farewell to the Caucasus: Regional ethnic clan politics and the growing instability of the ruling elite after the 2012 presidential elections in Russia",
    "The rise and fall of trade unionism in Zimbabwe, Part II: 1995-2000",
    "Lessons from the Crisis Ukraine, Russia, and the European Union",
    'Ethiopian foreign policy and the Ogaden War: the shift from "containment" to "destabilization," 1977-1991',
    "The Colonial Foundations of State Fragility and Failure",
    "State De-Construction in Iraq and Syria",
    "Apocalypse adjourned: the rise and decline of cold war environmentalism in Germany",
    "How Should We Now Conceptualize Protest, Diffusion, and Regime Change?",
    "Does natural gas fuel civil war? Rethinking energy security, international relations, and fossil-fuel conflict",
    "Right-wing populist parties and environmental politics: insights from the Austrian Freedom Party's support for the glyphosate ban",
]

domains_titles["L1D30"] = [
    "ECOLOGICAL RESTRUCTURING OR ENVIRONMENT FRIENDLY DEINDUSTRIALIZATION - THE FATE OF THE EAST GERMAN ENERGY SECTOR AND SOCIETY SINCE 1990",
    "Shared response to the market shocks?",
    "Capital market crises: liberalisation, fixed exchange rates and market-driven destabilisation",
    "Creative destruction or destructive perpetuation: The role of large state-owned enterprises and SMEs in Romania during transition",
    "Surviving the fall of a king: The regional institutional implications of crisis at fiat auto",
    "Bubble, Bust and More Boom: The Political Economy of Housing in Norway",
    "Mancur Olson and structural economic change: Vested interests and the industrial rise and fall of the great powers",
    "GOVERNANCE IN THE INTERESTS OF THE MOST VULNERABLE",
    "The Oil Climax: Can Nigeria's fuel subsidy reforms propel energy transitions?",
    'The town that said "No" to the Enbridge Northern Gateway pipeline: The Kitimat plebiscite of 2014',
    "Chipping away at democracy: Legislative slippage in Alberta's energy development zone",
    'From Digital Divides to Creative Destruction: Epistemological Encounters in the Regulation of the "Blood Mineral" Trade in the Congo',
    "Incumbent resistance and the solar transition: Changing opportunity structures and framing strategies",
    'Deindustrialization, "premature" deindustrialization, and "Dutch disease"',
    "Overtourism and degrowth: a social movements perspective",
    "Just energy transitions to low carbon economies: A review of the concept and its effects on labour and income",
    "Structural conflict under the new green dilemma: Inequalities in development of renewable energy for emerging economies",
]

domains_titles["L1D32"] = [
    "'We have the facts' - how business claims legitimacy in the environmental debate",
    "Negotiation and confrontation: Environmental policymaking through consensus",
    "The environmental movement and the modes of political action",
    "Reconceiving environmental justice: Global movements and political theories",
    "Scalar strategies in climate-change politics: debating the environmental consequences of a natural gas project",
    "Environmental justice and nuclear waste conflicts in Taiwan",
    "Crisis in policy, policy in crisis: understanding environmental discourse and resource-use conflict in northern Nigeria",
    "Green subjection: The politics of neoliberal urban environmental management",
    "Dangerous holes in global environmental governance: The roles of neoliberal discourse, science, and California agriculture in the Montreal Protocol",
    "A critical review of the successful CFC phase-out versus the delayed methyl bromide phase-out in the Montreal Protocol",
    "Court Decisions, NIMBY Claims, and the Siting of Unwanted Facilities: Policy Frames and the Impact of Judicialization in Locating a Landfill for Toronto's Solid Waste",
    "Uncomfortable knowledge: the social construction of ignorance in science and environmental policy discourses",
    "Diamond Mining in Canada's Northwest Territories: A Colonial Continuity",
    "SITUATING THE GREEN ECONOMY: DISCOURSES, COOPTATION, AND STATE CHANGE",
    "Historical paths of environmental injustice: a century of placing industrial facilities in Helsinki, Finland",
    "Environmental protests and NIMBY activism: Local politics and waste management in Beijing and Guangzhou",
    'Regulating global capitalism amid rampant corporate wrongdoing-Reply to "Three frames for innovation policy"',
    "Green Lawfare: Environmental Public Interest Litigation and Mediatized Environmental Conflict",
    "Environmental Crime and Contemporary Criminology: Making a Difference",
    "In and Against the State: The Dynamics of Environmental Activism",
    "Political ecology II: Whither the state?",
]


domains_titles["L1D33"] = [
    "ECONOMIC-CRISIS AND POLITICAL REGIME CHANGE - AN EVENT HISTORY ANALYSIS",
    "An overview of the political regime change dataset",
    "Democracy, regime change, and rivalry termination",
    "Regime changes and interstate conflict, 1816-1992",
    "Democratisation and the decline of social movements: The effects of regime change on collective action in Eastern Europe, Southern Europe and Latin America",
    "Regime change and system stability: Simulating complex anarchical environments over the Internet",
    "Regime cycles - Democracy, autocracy, and revolution in post-Soviet Eurasia",
    "Do economic sanctions destabilize country leaders?",
    "Information Cascades and Revolutionary Regime Transitions",
    "Foreign-Imposed Regime Change, State Power and Civil War Onset, 1920-2004",
    "The Patron's Dilemma: The Dynamics of Foreign-Supported Democratization",
    "Inequality and Regime Change: Democratic Transitions and the Stability of Democratic Rule",
    "Regime Change Cascades: What We Have Learned from the 1848 Revolutions to the 2011 Arab Uprisings",
    "Forced to Be Free? Why Foreign-Imposed Regime Change Rarely Leads to Democratization",
    "Macro shocks and costly political action in non-democracies",
    "Regime Type and Political Destabilization in Cross-National Perspective: A Re-Analysis",
    "The abolition of war as a goal of environmental policy",
    "Regime and Leader Instability Under Two Forms of Military Rule",
    "Measuring subnational democracy: toward improved regime typologies and theories of regime change",
    "Grievances, Mobilization, and Mass Opposition to Authoritarian Regimes: A Subnational Analysis of East Germany's 1953 Abbreviated Revolution",
    "Global patterns of regime change 1800-2015",
    "Elite defection and grassroots democracy under competitive authoritarianism: evidence from Burkina Faso",
    "Patterns of Regime Breakdown Since the French Revolution",
]

domains_titles["L1D34"] = [
    "POLITICAL COALITION BREAKING AND SUSTAINABILITY OF POLICY REFORM",
    "The EHCA 1985 (NSW): Historical perspective on issues arising in the control of toxic chemicals",
    "Environmental NGOs and regime change: The case of ocean dumping of radioactive waste",
    "The employment implications of a low-carbon economy",
    "State power, transformative capacity and adapting to globalization: an analysis of French agricultural policy, 1960-2000",
    "Repeal of the rice laws in Japan - The role of international pressure to overcome vested interests",
    "Framing nuclear waste as a political issue in France",
    "Crisis? What Crisis? The Normality of the Current Food Crisis",
    "Regulation of pesticides: A comparative analysis",
    "Blocking change: facing the drag of status quo fisheries institutions",
    "Creative destruction in urban planning procedures: the language of 'renewal' and 'exploitation'(21)",
    "Petrochemical Pollution and the Suppression of Environmental Protest",
    "Vested interests: Examining the political obstacles to power sector reform in twenty Indian states",
    "The dismantling of the rural and environmental public policies in Brazil",
]

domains_titles["L1D35"] = [
    "Pollution, political agendas, and policy windows: Environmental policy on the eve of Silent Spring",
    "Conflict and cooperation in environmental administration",
    "The ethics and politics of the caged layer hen debate in New Zealand",
    "States of Environmental Justice: Redistributive Politics across the United States, 1993-2004",
    "Evolution of the environmental justice movement: activism, formalization and differentiation",
    "Measuring environmental inequality",
    "Advocacy Coalition Resources and Strategies in Colorado Hydraulic Fracturing Politics",
    """Critical elements in implementing fundamental change in public environmental policy: Western Australia's mine closure and rehabilitation securities reform""",
    "ENVIRONMENTAL POLICY-MAKING AND TRIBUNAL DECISION-MAKING: ASSESSING THE SCOPE OF REGULATORY POWER IN INTERNATIONAL INVESTMENT ARBITRATION",
    "The Role of Legislation, Regulatory Initiatives and Guidelines on the Control of Plastic Pollution",
    "Lagging and Flagging: Air Pollution, Shale Gas Exploration and the Interaction of Policy, Science, Ethics and Environmental Justice in England",
    "Advocacy groups in China's environmental policymaking: Pathways to influence",
]

domains_titles["L1D36"] = [
    "The politics of pollution control in Brazil: State actors and social movements cleaning up Cubatao",
    "Business Lobbying and the Prospects for American Climate Change Legislation",
    'The Politics of "Fracking": Regulating Natural Gas Drilling Practices in Colorado and Texas',
    "Do rapporteurs receive independent expert policy advice? Indirect lobbying via the European Parliament's committee secretariat",
    "How do Competing Interest Groups Influence Environmental Policy? The Case of Renewable Electricity in Industrialized Democracies, 1989-2007",
    "The Environmental Policy of Charles I: Coal Smoke and the English Monarchy, 1624-40",
    "Polluting politics",
    "Fossil fuel reform in developing states: The case of Trinidad and Tobago, a petroleum producing small Island developing State",
    "Do environmental incidents affect state-level environmental building policy?",
    "A contested transition toward a coal-free future: Advocacy coalitions and coal policy in the Czech Republic",
    "The party politics of nuclear energy: Party cues and public opinion regarding nuclear energy in Belgium",
    "Oil and gas companies invest in legislators that vote against the environment",
]


def get_docs(corpus, domain_labels):
    docs = {}
    for label in domain_labels:
        titles = set(domains_titles.get(label, []))
        sel = corpus.data[corpus.col_title].isin(titles)
        print(titles.difference(corpus.data.loc[sel, corpus.col_title]))
        docs[label] = corpus.data.index[sel].to_list()
    return docs
