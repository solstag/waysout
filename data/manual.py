#!/usr/bin/env python

from pathlib import Path
from zipfile import Path as ZipPath

DATA_DIR = Path("data")


##############################################
# Manually downloaded core corpus inspection #
##############################################


def extract_tag(zpath, field_tag):
    tag_data = []
    for file in (zpath / x for x in zpath.root.namelist() if x.endswith(".txt")):
        tag_data.extend(
            line.split(maxsplit=1)[1]
            for line in file.read_text().splitlines()
            if line.startswith(field_tag)
        )
    print(len(tag_data), len(set(tag_data)))
    return set(tag_data)


def check_tag_overlap(field_tag="TI "):
    archive_paths = {
        "core": DATA_DIR / "corpus-coeur-141-articles-20-04-21.zip",
        "exte1": DATA_DIR / "extension-1-26-04-21.zip",
        "exte2": DATA_DIR / "extension-2-index-restreint-26-04-21-e.zip",
    }
    archives = {k: ZipPath(v) for k, v in archive_paths.items()}

    tag_data = {k: extract_tag(v, field_tag) for k, v in archives.items()}

    core = tag_data["core"]

    for k, v in tag_data.items():
        print(
            f"{k}: {len(v):,} documents,"
            f"{(_inter := len(v.intersection(core))):,} in core ({_inter / len(core):.0%} of core)",
            sep="\t",
        )
        print("\n".join(list(v)[:10]))
        print()
