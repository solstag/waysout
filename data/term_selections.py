terms = {}

terms["regimes_core"] = {
    # Terms from L1D28
    "crisis": "cris[ei]s",
    "disaster": "disasters?",
    "threat": "threats?",
    "erosion": "erosion|erod[A-z]+",
    "decay": "decay[A-z]*",
    "regression": "regression",
    "dysfunctional": "dysfunction[A-z]*",
    "failure": "fail[A-z]*",
    "drift": "drift[A-z]*",
    "retrenchment": "retrench[A-z]+",
    "dawn": "dawn",
    "breakdown": "breakdown",
    "dismantling": "dismantl[A-z]+",
    "death": "death",
    "vulnerability": "vulnerab[A-z]+",
    "precarity": "precarity",
    "disjuncture": "disjuncture",
    "instability": "instability",
    "conflict": "conflict[A-z]*",
    "demise": "demise",
    "fall": "fall[A-z]*",
    "decline": "declin[A-z]+",
    "disintegration": "disintegrat[A-z]+",
    "destabilisation": "destab[A-z]+",
    "removal": "remov[A-z]+",
}

terms["farmers_core"] = {
    # Terms from the three "farmers" domains
    "decline": "declin[A-z]+",
    "reduc": "reduc[A-z]+",
    "phase-out": "phas[A-z]+-out",
    "destabilisation": "destab[A-z]+",
    "abandon": "abandon[A-z]*",
    "discontinuity": "discontin[A-z]*",
    "crisis": "cris[ei]s",
    "controversy": "controvers[A-z]+",
    }
