import re

###################
# Term statistics #
###################


def term_stat(corpus, term_re):
    terms = corpus.data[corpus.column].map(lambda x: set([z for y in x for z in y]))
    return terms.map(lambda x: [y for y in x if re.search(term_re)])
