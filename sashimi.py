from pathlib import Path

import pandas as pd

from bibliodbs.biblio_wos import add_urls_from_ids

from sashimi import GraphModels
from sashimi.blocks.network_map import network_map

from .data import (
    add_extended_data,
    load_waysout_data,
    clean_data,
)
from .data.coded import get_docs_pertinence


def bootstrap(config_path=None):
    config_path = Path(config_path)
    if config_path.exists():
        raise ValueError(f"File {config_path} already exists")

    data = pd.DataFrame(list(load_waysout_data()))
    data.name = "waysout"
    add_urls_from_ids(data)

    corpus = GraphModels(
        config_path=config_path,
        text_source="abstract",
        labels=dict(
            id="uid",
            title="title",
            time="year",
            url=["url_doi", "url_wos"],
            venue="journal",
        ),
    )

    corpus.load_data(data)
    corpus.process_corpus(ngrams=3)
    corpus.data = clean_data(corpus.data, corpus.column)
    corpus.store_data()
    corpus.register_config()
    loaded_corpus = load(config_path=config_path)
    if corpus.data.equals(loaded_corpus.data):
        return loaded_corpus
    else:
        raise RuntimeError("Reloaded corpus differs from original")


def load(config_path=None, extend_prop="venue", extend_prop_name="journal"):
    config_path = Path(config_path)
    if config_path.exists():
        corpus = GraphModels(config_path=config_path)
    else:
        corpus = bootstrap(config_path=config_path)
    if extend_prop_name not in corpus.data:
        add_extended_data(corpus, extend_prop, extend_prop_name)
    return corpus


def dtm(corpus, load=True):
    register = corpus.get_blockstate() is None
    corpus.load_domain_topic_model(load=load)
    if register:
        corpus.register_config()


def dcm(corpus: GraphModels, prop, load=True):
    corpus.set_chain(prop=prop)
    register = corpus.get_blockstate(chained=True) is None
    corpus.load_domain_chained_model(load=load)
    if register:
        corpus.register_config()


def filter_pertinent(corpus):
    corpus.set_sample(get_docs_pertinence(corpus).astype(bool))


###########
# Results #
###########


def pertinence_report(corpus):
    pertinence_blocks = get_docs_pertinence(corpus).eq(1)
    print(pertinence_blocks.value_counts())
    dblocks = corpus.dblocks.loc[pertinence_blocks, 1].unique()
    network_map(corpus, "doc", 1, 1, split_on=pertinence_blocks, split_set={True})
    network_map(corpus, "doc", 1, 1, dblocks=dblocks)


def constellation_report(dblocks, corpus):
    corpus.set_sample()
