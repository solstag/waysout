"""
Ways-out
"""

from sashimi.blocks.tables import (
    l1domain_full_report,
    l1domains_code_terms_report,
    code_frequency_tables,
)

from . import data, sashimi, stats  # noqa
from .data import doc_selections, term_selections

DEFAULT_CONFIG_PATH = "auto_abstractology/reports/config.json"


def load_corpus():
    corpus = sashimi.load(DEFAULT_CONFIG_PATH)
    sashimi.dtm(corpus)

    # this would be better before the domain topic model, but it's too late now
    corpus.data[corpus.column] = data.filter_bad_terms(corpus)
    corpus.odata.loc[corpus.data.index, corpus.column] = corpus.data[corpus.column]

    return corpus


def main():
    # Full corpus
    corpus = load_corpus()
    # Interfaces
    corpus.domain_map("WAYS-OUT: domains ‹› topics")
    corpus.domain_network(doc_level=2, ter_level=1)
    corpus.domain_network(doc_level=1, ter_level=1)
    corpus.subxblocks_tables("doc", xlevel=3, xb=None, ybtype="ter")

    # Pertinent domains only
    sashimi.filter_pertinent(corpus)
    # Chain journals to pertinent domains
    sashimi.dcm(corpus, prop="journal")
    # Interfaces
    corpus.domain_map("WAYS-OUT: pertinent domains ‹› topics")
    corpus.domain_map("WAYS-OUT: pertinent domains ‹› journals", chained=True)
    corpus.domain_network(doc_level=2, ter_level=1)
    corpus.domain_network(doc_level=1, ter_level=1)
    corpus.domain_network(doc_level=2, ter_level=None, ext_level=1)
    corpus.domain_network(doc_level=1, ter_level=None, ext_level=1)
    corpus.domain_network(doc_level=2, ter_level=1, ext_level=1)
    for ybtype in ["ter", "ext"]:
        corpus.subxblocks_tables("doc", xlevel=3, xb=None, ybtype=ybtype)

    # Constellations
    for key, value in doc_selections.constellations:
        constellation_main(key, value)


def constellation_main(constellation_name, constellation_domains):
    corpus = load_corpus()
    sashimi.filter_pertinent(corpus)
    sashimi.dcm(corpus, prop="journal")
    constellation_dblocks_label = {
        corpus.label_to_hblock[domain_label][-1]: domain_label
        for domain_label in constellation_domains
    }
    for dblock, dlabel in constellation_dblocks_label.items():
        l1domain_full_report(
            corpus,
            dblock,
            "ter",
            outfile=corpus.blocks_adir
            / f"{constellation_name}-domain_contents-{dlabel}.html",
            plots=True,
        )
        l1domain_full_report(
            corpus,
            dblock,
            "ter",
            outfile=corpus.blocks_adir
            / f"{constellation_name}-domain_contents-{dlabel}-selected.html",
            plots=True,
            docs=doc_selections.get_docs(corpus, constellation_domains)[dlabel],
        )

    # Sample+trim on the constellation
    corpus.set_sample(corpus.dblocks[1].isin(constellation_dblocks_label))
    corpus.trim_to_sample()
    # Interfaces
    corpus.domain_network(doc_level=1, ter_level=1)
    corpus.domain_network(doc_level=1, ter_level=None, ext_level=1)
    corpus.domain_network(doc_level=1, ter_level=1, ext_level=1)
    for ybtype, adir in [("ter", corpus.blocks_adir), ("ext", corpus.chained_adir)]:
        corpus.xblocks_tables(
            "doc",
            xlevel_blocks=[(1, db) for db in constellation_dblocks_label],
            ybtype=ybtype,
            outfile=adir / f"{constellation_name}-domain_table-{ybtype}.html",
        )


def constellation_TODO(corpus, constellation_name, term_selections_name):
    name_prefix = f"{constellation_name}-terms_{term_selections_name}"
    constellation_doc_selection = doc_selections.constellations[constellation_name]
    constellation_term_selection = term_selections.terms[term_selections_name]
    code_frequency_tables(
        corpus,
        constellation_doc_selection,
        constellation_term_selection,
        name_prefix,
    )
    # return
    for dl in constellation_doc_selection:
        l1domain_full_report(
            corpus,
            corpus.label_to_tlblock[dl][-1],
            "ter",
            outfile=corpus.blocks_adir / f"{dl}-terms_{term_selections_name}.html",
            plots=True,
            code_terms_map=constellation_term_selection,
        )
    l1domains_code_terms_report(
        corpus,
        [corpus.label_to_tlblock[dl][1:] for dl in constellation_doc_selection],
        "ter",
        outdir=corpus.blocks_adir / f"{name_prefix}",
        plots=True,
        code_terms_map=constellation_term_selection,
    )
